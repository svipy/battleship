const assert = require("assert").strict;
const battleship = require("../battleship.js");
const letters = require("../GameController/letters.js");
const position = require("../GameController/position.js");

describe("parsePositionTests", function () {
	it("should return a valid position for valid input B3 down", function () {
		var expected = [new position(letters.B, 3), 'down'];
		var actual = battleship.ParsePosition("B3 down");
		assert.deepStrictEqual(actual, expected);
	});

	it("should return a valid position for valid input B3 top", function () {
		var expected = [new position(letters.B, 3), 'top'];
		var actual = battleship.ParsePosition("B3 top");
		assert.deepStrictEqual(actual, expected);
	});

	it("should return a valid position for invalid input B3 wrong", function () {
		var expected = 'error';
		var actual = battleship.ParsePosition("B3 wrong");
		assert.deepStrictEqual(actual, expected);
	});
	
	it("should return a valid position for invalid input BZ top", function () {
		var expected = 'error';
		var actual = battleship.ParsePosition("3k top");
		assert.deepStrictEqual(actual, expected);
	});
		
	it("should return a valid position for invalid input empty string", function () {
		var expected = 'error';
		var actual = battleship.ParsePosition("");
		assert.deepStrictEqual(actual, expected);
	});
});
