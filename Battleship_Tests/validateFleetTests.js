const assert = require("assert").strict;
const battleship = require("../battleship.js");
const letters = require("../GameController/letters.js");
const position = require("../GameController/position.js");
const gameController = require("../GameController/gameController.js");

describe("validateFleetTests", function () {
	it("should return string error if gets null", function () {
		var battle = new battleship();
		var error = battle.ValidateShip(null);
		assert.deepStrictEqual(error, "Out of bounds.");
	});

	it("should return null if gets first ship", function () {
		var battle = new battleship();
		battle.myFleet = gameController.InitializeShips();

		var error = battle.ValidateShip([
			new position(letters.A, 1),
			new position(letters.A, 2),
			new position(letters.A, 3),
			new position(letters.A, 4),
		]);
		assert.deepStrictEqual(error, null);
	});

	it("should return string error if ships cross", function () {
		var battle = new battleship(); // TODO: parmas
		battle.myFleet = gameController.InitializeShips();

		battle.myFleet[0].addPosition(new position(letters.A, 1));
		battle.myFleet[0].addPosition(new position(letters.A, 2));
		battle.myFleet[0].addPosition(new position(letters.A, 3));
		battle.myFleet[0].addPosition(new position(letters.A, 4));
		battle.myFleet[0].addPosition(new position(letters.A, 5));

		var error = battle.ValidateShip([
			new position(letters.A, 1),
			new position(letters.B, 1),
		]);
		assert.deepStrictEqual(error, "Ships cross");
	});

	it("should return null if ships dont cross", function () {
		var battle = new battleship(); // TODO: parmas
		battle.myFleet = gameController.InitializeShips();

		battle.myFleet[0].addPosition(new position(letters.A, 1));
		battle.myFleet[0].addPosition(new position(letters.A, 2));
		battle.myFleet[0].addPosition(new position(letters.A, 3));
		battle.myFleet[0].addPosition(new position(letters.A, 4));
		battle.myFleet[0].addPosition(new position(letters.A, 5));

		var error = battle.ValidateShip([
			new position(letters.B, 1),
			new position(letters.B, 2),
		]);
		assert.deepStrictEqual(error, null);
	});

});
