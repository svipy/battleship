const readline = require("readline-sync");
const gameController = require("./GameController/gameController.js");
const cliColor = require("cli-color");
const beep = require("beepbeep");
const position = require("./GameController/position.js");
const letters = require("./GameController/letters.js");
const directions = ['left', 'top', 'down', 'right'];
class Battleship {
  start() {
    console.log(cliColor.magenta("                                     |__"));
    console.log(cliColor.magenta("                                     |\\/"));
    console.log(cliColor.magenta("                                     ---"));
    console.log(cliColor.magenta("                                     / | ["));
    console.log(cliColor.magenta("                              !      | |||"));
    console.log(
      cliColor.magenta("                            _/|     _/|-++'")
    );
    console.log(
      cliColor.magenta("                        +  +--|    |--|--|_ |-")
    );
    console.log(
      cliColor.magenta("                     { /|__|  |/\\__|  |--- |||__/")
    );
    console.log(
      cliColor.magenta(
        "                    +---------------___[}-_===_.'____                 /\\"
      )
    );
    console.log(
      cliColor.magenta(
        "                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _"
      )
    );
    console.log(
      cliColor.magenta(
        " __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7"
      )
    );
    console.log(
      cliColor.magenta(
        "|                        Welcome to Battleship                         BB-61/"
      )
    );
    console.log(
      cliColor.magenta(
        " \\_________________________________________________________________________|"
      )
    );
    console.log();

    this.InitializeGame();
    this.StartGame();
  }

  StartGame() {
    console.clear();
    console.log("                  __");
    console.log("                 /  \\");
    console.log("           .-.  |    |");
    console.log("   *    _.-'  \\  \\__/");
    console.log("    \\.-'       \\");
    console.log("   /          _/");
    console.log("  |      _  /");
    console.log("  |     /_\\'");
    console.log("   \\    \\_/");
    console.log('    """"');

    do {
      console.log();
      console.log("Player, it's your turn");
      console.log("Enter coordinates for your shot :");
      var position = Battleship.ParsePosition(readline.question());
      var isHit = gameController.CheckIsHit(this.enemyFleet, position);
      if (isHit) {
        beep();

        console.log("                \\         .  ./");
        console.log('              \\      .:";\'.:.."   /');
        console.log("                  (M^^.^~~:.'\").");
        console.log("            -   (/  .    . . \\ \\)  -");
        console.log("               ((| :. ~ ^  :. .|))");
        console.log("            -   (\\- |  \\ /  |  /)  -");
        console.log("                 -\\  \\     /  /-");
        console.log("                   \\  \\   /  /");
      }

      console.log(isHit ? "Yeah ! Nice hit !" : "Miss");

      var computerPos = this.GetRandomPosition();
      var isHit = gameController.CheckIsHit(this.myFleet, computerPos);
      console.log();
      console.log(
        `Computer shot in ${computerPos.column}${computerPos.row} and ` +
          (isHit ? `has hit your ship !` : `miss`)
      );
      if (isHit) {
        beep();

        console.log("                \\         .  ./");
        console.log('              \\      .:";\'.:.."   /');
        console.log("                  (M^^.^~~:.'\").");
        console.log("            -   (/  .    . . \\ \\)  -");
        console.log("               ((| :. ~ ^  :. .|))");
        console.log("            -   (\\- |  \\ /  |  /)  -");
        console.log("                 -\\  \\     /  /-");
        console.log("                   \\  \\   /  /");
      }
    } while (true);
  }

  static ParsePosition(input) {
    var letter = letters.get(input.toUpperCase().substring(0, 1));
    var number = parseInt(input.substring(1, 2), 10);
    if(Number.isNaN(number)) {
      return 'error';
    }
    var direction = input.substring(3, input.length);
    
    if (!directions.includes(direction)) {
      return 'error';
    }

    return [new position(letter, number), direction];
  }

  GetRandomPosition() {
    var rows = 8;
    var lines = 8;
    var rndColumn = Math.floor(Math.random() * lines);
    var letter = letters.get(rndColumn + 1);
    var number = Math.floor(Math.random() * rows);
    var result = new position(letter, number);
    return result;
  }

  InitializeGame() {
    this.InitializeMyFleet();
    this.InitializeRandomEnemyFleet();
  }

  InitializeMyFleet() {
    this.myFleet = gameController.InitializeShips();

    console.log(
      "Please position your fleet (Game board size is from A to H and 1 to 8) :"
    );

    this.myFleet.forEach(function (ship) {
      console.log();
      console.log(`Please enter the position and direction for the ${ship.name} (size: ${ship.size})`);
      console.log(`Enter position and direction of ${ship.size} (i.e A3 down):`);
      do {
        const input = readline.question();
        const parsedInput = Battleship.ParsePosition(input);
        if(parsedInput === 'error') {
          console.log(`bad position or direction`); 
          continue;
        }

        const [position, direction] = parsedInput;
        const points = generatePoints(position, direction, ship.size);
        const error = this.ValidateShip(points);
    
        if(error) {
          console.log(`something going wrong: ${error}`); 
          continue;
        }
    
        for (var i = 1; i < ship.size + 1; i++) {
          ship.addPosition(points[i - 1]);
        }
      
        break;
      } while(true);
    });
  }

  ValidateShip(points) {
    if (!points) {
      return "Out of bounds."
    }

    let result = null;
    this.myFleet.forEach(ship => {
    
      ship.positions.forEach(position => {
        let a = points.some(point => point.column === position.column && point.row === position.row);
        if (a) {
          result = "Ships cross";
        }
      });
    });
    
    return result;

	}

  InitializeRandomEnemyFleet() {
    const enemyFleetFactories = [
      this.InitializeEnemyFleet1,
      this.InitializeEnemyFleet2,
      this.InitializeEnemyFleet3,
      this.InitializeEnemyFleet4,
      this.InitializeEnemyFleet5,
    ];
    const randomNumber = this.getRandomNumber(enemyFleetFactories.length);
    const enemyFleetFactory = enemyFleetFactories[randomNumber];

    if (typeof enemyFleetFactory === "function") {
      enemyFleetFactory.call(this);
    } else {
      throw Error("bad enemy factory");
    }
  }

  getRandomNumber(max) {
    return Math.floor(Math.random() * max);
  }

  InitializeEnemyFleet1() {
    this.enemyFleet = gameController.InitializeShips();

    this.enemyFleet[0].addPosition(new position(letters.A, 8));
    this.enemyFleet[0].addPosition(new position(letters.B, 8));
    this.enemyFleet[0].addPosition(new position(letters.C, 8));
    this.enemyFleet[0].addPosition(new position(letters.D, 8));
    this.enemyFleet[0].addPosition(new position(letters.E, 8));

    this.enemyFleet[1].addPosition(new position(letters.G, 3));
    this.enemyFleet[1].addPosition(new position(letters.G, 4));
    this.enemyFleet[1].addPosition(new position(letters.G, 5));
    this.enemyFleet[1].addPosition(new position(letters.G, 6));

    this.enemyFleet[2].addPosition(new position(letters.C, 3));
    this.enemyFleet[2].addPosition(new position(letters.C, 4));
    this.enemyFleet[2].addPosition(new position(letters.C, 5));

    this.enemyFleet[3].addPosition(new position(letters.E, 1));
    this.enemyFleet[3].addPosition(new position(letters.F, 1));
    this.enemyFleet[3].addPosition(new position(letters.G, 1));

    this.enemyFleet[4].addPosition(new position(letters.C, 1));
    this.enemyFleet[4].addPosition(new position(letters.C, 2));
  }
  InitializeEnemyFleet2() {
    this.enemyFleet = gameController.InitializeShips();

    this.enemyFleet[0].addPosition(new position(letters.A, 1));
    this.enemyFleet[0].addPosition(new position(letters.A, 2));
    this.enemyFleet[0].addPosition(new position(letters.A, 3));
    this.enemyFleet[0].addPosition(new position(letters.A, 4));
    this.enemyFleet[0].addPosition(new position(letters.A, 5));

    this.enemyFleet[1].addPosition(new position(letters.C, 5));
    this.enemyFleet[1].addPosition(new position(letters.C, 6));
    this.enemyFleet[1].addPosition(new position(letters.C, 7));
    this.enemyFleet[1].addPosition(new position(letters.C, 8));

    this.enemyFleet[2].addPosition(new position(letters.E, 3));
    this.enemyFleet[2].addPosition(new position(letters.F, 3));
    this.enemyFleet[2].addPosition(new position(letters.G, 3));

    this.enemyFleet[3].addPosition(new position(letters.E, 8));
    this.enemyFleet[3].addPosition(new position(letters.F, 8));
    this.enemyFleet[3].addPosition(new position(letters.G, 8));

    this.enemyFleet[4].addPosition(new position(letters.D, 1));
    this.enemyFleet[4].addPosition(new position(letters.E, 1));
  }
  InitializeEnemyFleet3() {
    this.enemyFleet = gameController.InitializeShips();

    this.enemyFleet[0].addPosition(new position(letters.D, 8));
    this.enemyFleet[0].addPosition(new position(letters.E, 8));
    this.enemyFleet[0].addPosition(new position(letters.F, 8));
    this.enemyFleet[0].addPosition(new position(letters.G, 8));
    this.enemyFleet[0].addPosition(new position(letters.H, 8));

    this.enemyFleet[1].addPosition(new position(letters.E, 1));
    this.enemyFleet[1].addPosition(new position(letters.F, 1));
    this.enemyFleet[1].addPosition(new position(letters.G, 1));
    this.enemyFleet[1].addPosition(new position(letters.H, 1));

    this.enemyFleet[2].addPosition(new position(letters.B, 1));
    this.enemyFleet[2].addPosition(new position(letters.B, 2));
    this.enemyFleet[2].addPosition(new position(letters.B, 3));

    this.enemyFleet[3].addPosition(new position(letters.C, 3));
    this.enemyFleet[3].addPosition(new position(letters.C, 4));
    this.enemyFleet[3].addPosition(new position(letters.C, 5));

    this.enemyFleet[4].addPosition(new position(letters.F, 4));
    this.enemyFleet[4].addPosition(new position(letters.G, 4));
  }
  InitializeEnemyFleet4() {
    this.enemyFleet = gameController.InitializeShips();

    this.enemyFleet[0].addPosition(new position(letters.B, 2));
    this.enemyFleet[0].addPosition(new position(letters.C, 2));
    this.enemyFleet[0].addPosition(new position(letters.D, 2));
    this.enemyFleet[0].addPosition(new position(letters.E, 2));
    this.enemyFleet[0].addPosition(new position(letters.F, 2));

    this.enemyFleet[1].addPosition(new position(letters.B, 4));
    this.enemyFleet[1].addPosition(new position(letters.B, 5));
    this.enemyFleet[1].addPosition(new position(letters.B, 6));
    this.enemyFleet[1].addPosition(new position(letters.B, 7));

    this.enemyFleet[2].addPosition(new position(letters.D, 5));
    this.enemyFleet[2].addPosition(new position(letters.D, 6));
    this.enemyFleet[2].addPosition(new position(letters.D, 7));

    this.enemyFleet[3].addPosition(new position(letters.F, 6));
    this.enemyFleet[3].addPosition(new position(letters.G, 6));
    this.enemyFleet[3].addPosition(new position(letters.H, 6));

    this.enemyFleet[4].addPosition(new position(letters.E, 4));
    this.enemyFleet[4].addPosition(new position(letters.F, 4));
  }
  InitializeEnemyFleet5() {
    this.enemyFleet = gameController.InitializeShips();

    this.enemyFleet[0].addPosition(new position(letters.E, 2));
    this.enemyFleet[0].addPosition(new position(letters.E, 3));
    this.enemyFleet[0].addPosition(new position(letters.E, 4));
    this.enemyFleet[0].addPosition(new position(letters.E, 5));
    this.enemyFleet[0].addPosition(new position(letters.E, 6));

    this.enemyFleet[1].addPosition(new position(letters.B, 7));
    this.enemyFleet[1].addPosition(new position(letters.C, 7));
    this.enemyFleet[1].addPosition(new position(letters.E, 7));
    this.enemyFleet[1].addPosition(new position(letters.F, 7));

    this.enemyFleet[2].addPosition(new position(letters.H, 2));
    this.enemyFleet[2].addPosition(new position(letters.H, 3));
    this.enemyFleet[2].addPosition(new position(letters.H, 4));

    this.enemyFleet[3].addPosition(new position(letters.F, 8));
    this.enemyFleet[3].addPosition(new position(letters.G, 8));
    this.enemyFleet[3].addPosition(new position(letters.H, 8));

    this.enemyFleet[4].addPosition(new position(letters.B, 2));
    this.enemyFleet[4].addPosition(new position(letters.B, 3));
  }
}

module.exports = Battleship;
